## 目錄結構

- mysql
	- data #系統自動備份的 mysql 資料
	- Dockerfile #建置 mysql 的 Dockerfile
- php
	- conf
		- sites-available
			- 000-default.conf #apache 預設設定(default conf)
	- Dockerfile #建置 php 的 Dockerfile
- www #專案放置位子
	- index.php #預設頁面

## Docker 配置分三個容器

1. php-8 + apache:80 
2. mysql-8:3306
3. phpmyadmin:8080

> 1為主要容器，容器名稱預設為 main 可在 docker-compose 內自定義(phpapache底下的container_name) 

#### 如何啟用容器 ?
> 在此專案根目錄執行 docker-compose up

#### 專案放置位置在哪？
> 預想位置在 /www 內

#### 如何設定 apache 指向指定的專案目錄？
> 設定 php/conf/sites-available/000-default.conf 檔

#### 如何在容器內下指令 ?
> docker exec -it {容器名稱} /bin/bash 

#### 如何得知當前執行容器與其名稱 ?
> docker ps

#### 如何創建 laravel 專案 ?
> composer create-project laravel/laravel laravel-9

*註：[容器1]包含composer*

##引用

[利用 Dockfile、Docker Compose 建立 LAMP 環境 (PHP、Apache、MySQL)](https://hackmd.io/@titangene/docker-lamp)